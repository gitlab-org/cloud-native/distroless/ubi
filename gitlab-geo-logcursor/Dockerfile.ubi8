## FINAL IMAGE ##

ARG RAILS_IMAGE=

FROM ${RAILS_IMAGE}

ARG GITLAB_VERSION
ARG UID=1000
ARG FIPS_MODE=0

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-geo-logcursor" \
      name="GitLab Geo Log Cursor" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Geo Log Cursor daemon." \
      description="Geo Log Cursor daemon."

# Control the target for `wait-for-deps` schema check
ENV DB_SCHEMA_TARGET=geo

COPY --chown=0:0 --chmod=755 scripts/ /scripts/

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

USER ${UID}

CMD ["/scripts/process-wrapper"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
