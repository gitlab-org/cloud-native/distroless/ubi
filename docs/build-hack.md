[[_TOC_]]

# Experimental Build Strategies

This is documentation for experimental strategies to determine runtime requirements of final image layer context.

Related Issues

- [#11](https://gitlab.com/gitlab-org/cloud-native/distroless/ubi/-/issues/11)

## Slim

[Slim](https://github.com/slimtoolkit/slim) provides a means to dynamically analyze processes and files accessed in a temporarily running "analysis container" based on the original container you are interested in minimizing by injecting and running a "sensor" executable. This strategy may provide useful information when determining minimal image content size and help identify what system packages/libraries are needed.

By default, the tool will run the ENTRYPOINT/CMD declared in the original image to test the temporary analysis container and allow the "sensor" to identify artifacts, but more comprehensive integrations tests are likely needed to determine what other files/libraries the context needs.

### Slim Build

The following example demonstrates various options used while exploring how to run integration tests for a particular context during a `slim build`. This may provide insight into how such a strategy might be explored further for other contexts.

As identified by https://gitlab.com/gitlab-org/cloud-native/distroless/ubi/-/issues/63#note_1897143686 I decided to try and run the gitlab-pages project's golang acceptance tests from within the gitlab-pages final image when doing a slim build.

```sh
git clone git@gitlab.com:gitlab-org/gitlab-pages.git

cd gitlab-pages

# this puts a compiled go test in the correct location for it to work (the project relies on relative paths)
CGO_ENABLED=0 go run gotest.tools/gotestsum@v1.10.0 --junitfile junit-test-report.xml --format testname -- -v ./test/acceptance -c -o ./test/acceptance/foo 

# create a link in the gitlab-pages directory to point to the location of the gitlab-pages binary in the actual "fat image". This is needed since the tests expect the binary to exist in a certain location
ln -s /usr/local/bin/gitlab-pages ./gitlab-pages

cd ..
```

Define an executable script to run the tests called gitlab-pages-probe.sh

```sh gitlab-pages-probe.sh
cat <<EOF >> gitlab-pages-probe.sh
#!/bin/bash

# Example test commands. These should persist in the slimmed image.
set -x

cd /gitlab-pages/test/acceptance

./foo || true
EOF
```

Define a target image for slim to work on...

```sh
# example target the image built by a pipeline
export TARGET_IMAGE_FOR_SLIM="registry.gitlab.com/gitlab-org/cloud-native/distroless/ubi/gitlab-pages:docs-runtime-requirements-methods-ubi8"
```

Note the current target image size to slim...

```sh
[tbox@fedora gitlab-org]$ docker image inspect ${TARGET_IMAGE_FOR_SLIM} -f '{{ .Size }}'
179618303
```

Run the slim build and mount the source code with the compiled go test binary AND symbolic link...

```sh
# run acceptance tests while slimming
slim build --http-probe-off --continue-after=1 --tag gitlab-pages-acceptance.slim --exec-file gitlab-pages-probe.sh  --target ${TARGET_IMAGE_FOR_SLIM} --mount $(pwd)/gitlab-pages/:/gitlab-pages/ --copy-meta-artifacts . > gitlab-pages-acceptance.slim.build.log
```

> Notice there is a [Container Report](https://github.com/slimtoolkit/slim/issues/534#issuecomment-2062536745) "creport.json" artifact that is created after the build that is a log of what files had been detected by the Slim sensor during testing

[gitlab-pages-acceptance.slim.build.log](./.build-hack-files/gitlab-pages-acceptance.slim.build.log)

retain the creport.json to compare against a normal slim build (no accept tests)...

```sh
mv creport.json creport-gitlab-pages-acceptance.slim.json
```

slim build with defaults for comparison...

```sh
slim build --http-probe-off --continue-after=1 --tag gitlab-pages-base.slim --target ${TARGET_IMAGE_FOR_SLIM} --copy-meta-artifacts . > gitlab-pages-base.slim.build.log
```

[gitlab-pages-base.slim.build.log](./.build-hack-files/gitlab-pages-base.slim.build.log)

retain the creport.json to compare against previous slim build that used the acceptance tests...

```sh
mv creport.json creport-gitlab-pages-base.slim.json
```

Next lets see the differences...

image sizes (not much, but still different)...

```sh
[tbox@fedora gitlab-org]$ docker image inspect gitlab-pages-acceptance.slim -f '{{ .Size }}'
85751234
[tbox@fedora gitlab-org]$ docker image inspect gitlab-pages-base.slim -f '{{ .Size }}'
80992076
```

Additionally, you could also use a diffing tool like [container-diff](https://github.com/GoogleContainerTools/container-diff) to spot the file differences...

```sh
container-diff diff daemon://gitlab-pages-acceptance.slim daemon://gitlab-pages-base.slim -t file -w out.txt
```

[out.txt](./.build-hack-files/out.txt)

There are some slight differences, lots of small files in /tmp during the test and the following "interesting" files that the tests used (and would have otherwise been removed)...

```text diff
These entries have been deleted from gitlab-pages-acceptance.slim:
FILE                                                                SIZE
/etc/group                                                          264B
/scripts/healthcheck                                                52B
/tmp/gitlab-api-secret1023217327                                    44B
...
/usr/lib/.build-id/49                                               35B
/usr/lib/.build-id/49/81e18ca15b47b83bbfe374e4abfd622fd54457        35B
/usr/lib/.build-id/e0/8f397aa6b7de799209cd5bc35aabe0496678f1        36B
/usr/lib64/libpthread.so.0                                          18B
```

compare all three images...

```sh
[tbox@fedora gitlab-org]$ docker images
REPOSITORY                                                                TAG                                      IMAGE ID       CREATED             SIZE
gitlab-pages-base.slim                                                    latest                                   6d12760e2b3b   2 minutes ago       81MB
gitlab-pages-acceptance.slim                                              latest                                   c163879a576b   4 minutes ago       85.8MB
registry.gitlab.com/gitlab-org/cloud-native/distroless/ubi/gitlab-pages   docs-runtime-requirements-methods-ubi8   4a0ce9bd4808   21 hours ago        180MB
```

Finally the creport files...

[creport-gitlab-pages-acceptance.slim.json](./.build-hack-files/creport-gitlab-pages-acceptance.slim.json)

[creport-gitlab-pages-base.slim.json](./.build-hack-files/creport-gitlab-pages-base.slim.json)

Hopefully this demonstrates how testing may help identify what libraries are needed during a slimming process (per context). To take this approach to the next step would require working closer with developers who maintain a context's testing strategy and standardized automation to build and run the tests as part of in image build pipeline.

Another interesting read https://elatov.github.io/2022/03/using-docker-slim-to-reduce-the-size-a-docker-image/
