ARG GITLAB_BASE_IMAGE
ARG UBI_IMAGE=registry.access.redhat.com/ubi8/ubi-minimal:8.9

FROM ${GITLAB_BASE_IMAGE} as target

FROM ${UBI_IMAGE} as build

ARG DNF_OPTS
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_INSTALL_ROOT=/install-root
ARG DNF_OPTS_ROOT

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target / ${DNF_INSTALL_ROOT}/

ADD gitlab-shell.tar.gz ${DNF_INSTALL_ROOT}/
ADD gitlab-logger.tar.gz ${DNF_INSTALL_ROOT}/usr/local/bin
ADD gitlab-gomplate.tar.gz ${DNF_INSTALL_ROOT}/

COPY scripts/ ${DNF_INSTALL_ROOT}/scripts/
COPY sshd_config ${DNF_INSTALL_ROOT}/etc/ssh/

RUN mkdir -p ${DNF_INSTALL_ROOT}/srv/sshd ${DNF_INSTALL_ROOT}/var/log/gitlab-shell \
    && touch ${DNF_INSTALL_ROOT}/var/log/gitlab-shell/gitlab-shell.log \
    && chown -R ${UID}:0 ${DNF_INSTALL_ROOT}/srv/sshd ${DNF_INSTALL_ROOT}/srv/gitlab-shell ${DNF_INSTALL_ROOT}/var/log/gitlab-shell ${DNF_INSTALL_ROOT}/etc/ssh ${DNF_INSTALL_ROOT}/scripts \
    && chmod -R g=u ${DNF_INSTALL_ROOT}/srv/sshd ${DNF_INSTALL_ROOT}/srv/gitlab-shell ${DNF_INSTALL_ROOT}/var/log/gitlab-shell ${DNF_INSTALL_ROOT}/etc/ssh ${DNF_INSTALL_ROOT}/scripts

RUN microdnf update -y && \
    microdnf ${DNF_OPTS} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 shadow-utils && \
    adduser -m ${GITLAB_USER} -u ${UID} -R ${DNF_INSTALL_ROOT}/

## Note: Not installing "procps" package since it is already in the GITLAB_BASE_IMAGE image
RUN microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 \
    fipscheck-lib openssh openssh-server \
    && microdnf ${DNF_OPTS_ROOT} clean all \
    && rm -f ${DNF_INSTALL_ROOT}/usr/libexec/openssh/ssh-keysign \
    && chown ${UID}:0 ${DNF_INSTALL_ROOT}/etc/ssh \
    && install -o 0 -g 0 -m 0755 ${DNF_INSTALL_ROOT}/scripts/authorized_keys ${DNF_INSTALL_ROOT}/authorized_keys \
    && install -d -o ${UID} -g 0 -m 770 ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} \
    && rm -f ${DNF_INSTALL_ROOT}/var/lib/dnf/history*

## FINAL IMAGE ##

FROM ${GITLAB_BASE_IMAGE}

ARG UID=1000
ARG GITLAB_SHELL_VERSION
ARG FIPS_MODE=0
ARG DNF_INSTALL_ROOT=/install-root

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-shell" \
      name="GitLab Shell" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_SHELL_VERSION} \
      release=${GITLAB_SHELL_VERSION} \
      summary="SSH access and repository management app for GitLab." \
      description="SSH access and repository management app for GitLab."

COPY --from=build ${DNF_INSTALL_ROOT}/ /

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

ENV CONFIG_TEMPLATE_DIRECTORY=/srv/gitlab-shell

USER ${UID}

CMD ["/scripts/process-wrapper"]

VOLUME /var/log/gitlab-shell

HEALTHCHECK --interval=10s --timeout=3s --retries=3 CMD /scripts/healthcheck
