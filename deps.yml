version: 3
dependencies:
- type: git
  manifest_updates:
    filters:
    - name: .* # everything gets it's own MR
      enabled: true
  settings:
    # Assign to maintainers
    # gitlab_assignee_ids:
      ## maintainers
      # - 597578 # WarheadsSE (jplum)
      # - 4849   # balasankarc (balu)
      # - 2602134 # rmarshall (*)
      ## trainee maintainers
      # - 888551 # pursultani
      # - 12300535 # apatterson2
    commit_message_template: |-
      {{.SubjectAndBody}}

      Changelog: changed

      [ci skip]
    gitlab_labels:
      - group::distribution
      - section::core platform
      - devops::systems
      - type::maintenance
      - maintenance::dependency
      - dependencies.io
      - workflow::ready for review
    gitlab_remove_source_branch: true
    remotes:
      # AWS CLI
      https://github.com/aws/aws-cli.git:
        replace_in_files:
        - filename: ci_files/variables.yml
          pattern: 'AWSCLI_VERSION: "(\S+)"'
          range: '< 2.0.0'
        - filename: gitlab-toolbox/Dockerfile
          pattern: 'ARG AWSCLI_VERSION="(\S+)"'
          range: '< 2.0.0'
        - filename: gitlab-toolbox/Dockerfile.build.ubi8
          pattern: 'ARG AWSCLI_VERSION=(\S+)'
          range: '< 2.0.0'
      # s3cmd
      https://github.com/s3tools/s3cmd.git:
        replace_in_files:
        - filename: ci_files/variables.yml
          pattern: 'S3CMD_VERSION: "(\S+)"'
          range: '>= 2.x.x'
        - filename: gitlab-toolbox/Dockerfile
          pattern: 'ARG S3CMD_VERSION="(\S+)"'
          range: '>= 2.x.x'
        - filename: gitlab-toolbox/Dockerfile.build.ubi8
          pattern: 'ARG S3CMD_VERSION=(\S+)'
          range: '>= 2.x.x'
      # YQ
      https://github.com/mikefarah/yq.git:
        replace_in_files:
        - filename: ci_files/variables.yml
          pattern: 'YQ_VERSION: "(\S+)"'
          range: '>= 4.x.x'
        - filename: kubectl/Dockerfile
          pattern: 'ARG YQ_VERSION="(\S+)"'
          range: '>= 4.x.x'
        - filename: kubectl/Dockerfile.build.ubi8
          pattern: 'ARG YQ_VERSION=(\S+)'
          range: '>= 4.x.x'
      # GitLab Exporter
      https://gitlab.com/gitlab-org/gitlab-exporter.git:
        replace_in_files:
        - filename: ci_files/variables.yml
          pattern: 'GITLAB_EXPORTER_VERSION: "(\S+)"'
        - filename: gitlab-exporter/Dockerfile
          pattern: 'ARG GITLAB_EXPORTER_VERSION=(\S+)'
        - filename: gitlab-exporter/Dockerfile.build.ubi8
          pattern: 'ARG GITLAB_EXPORTER_VERSION=(\S+)'
      # rubygems and bundler
      https://github.com/rubygems/rubygems.git:
        replace_in_files:
        - &rubygems-replace
          filename: gitlab-ruby/Dockerfile
          pattern: 'ARG RUBYGEMS_VERSION=3.(\S+)'
          tag_filter:
            matching: 'v(\d+).(\d+).(\d+)'
            sort_as: '$1.$2.$3'
            output_as: '$2.$3'
        - <<: *rubygems-replace
          filename: gitlab-ruby/Dockerfile.build.ubi8
          pattern: 'ARG RUBYGEMS_VERSION=3.(\S+)'
        - <<: *rubygems-replace
          filename: gitlab-ruby/Dockerfile
          pattern: 'ARG BUNDLER_VERSION=2.(\S+)'
        - <<: *rubygems-replace
          filename: gitlab-ruby/Dockerfile.build.ubi8
          pattern: 'ARG BUNDLER_VERSION=2.(\S+)'
      # redis gem
      https://github.com/redis/redis-rb.git:
        replace_in_files:
        - filename: gitlab-mailroom/scripts/install-dependencies
          pattern: 'redis:(\S+)'
          tag_prefix: 'v'
        - filename: gitlab-mailroom/Dockerfile.build.ubi8
          pattern: 'redis:(\S+)'
          tag_prefix: 'v'
      # redis client gem
      https://github.com/redis-rb/redis-client.git:
        replace_in_files:
        - filename: gitlab-mailroom/scripts/install-dependencies
          pattern: 'redis-client:(\S+)'
          tag_prefix: 'v'
        - filename: gitlab-mailroom/Dockerfile.build.ubi8
          pattern: 'redis-client:(\S+)'
          tag_prefix: 'v'
      # GitLab Container Registry
      https://gitlab.com/gitlab-org/container-registry.git:
        replace_in_files:
        - filename: ci_files/variables.yml
          pattern: 'GITLAB_CONTAINER_REGISTRY_VERSION: "(\S+)"'
          tag_filter:
            matching: 'v(\S+)-gitlab'
            sort_as: '$1'
        - filename: gitlab-container-registry/Dockerfile
          pattern: 'ARG REGISTRY_VERSION=(\S+)'
          tag_filter:
            matching: 'v(\S+)-gitlab'
            sort_as: '$1'
        - filename: gitlab-container-registry/Dockerfile.build.ubi8
          pattern: 'ARG REGISTRY_VERSION=(\S+)'
          tag_filter:
            matching: 'v(\S+)-gitlab'
            sort_as: '$1'
      # gomplate
      https://github.com/hairyhenderson/gomplate.git:
        replace_in_files:
        - filename: gitlab-gomplate/Dockerfile
          pattern: 'ARG GOMPLATE_VERSION="v(\S+)"'
          tag_prefix: 'v'
        - filename: gitlab-gomplate/Dockerfile.build.ubi8
          pattern: 'ARG GOMPLATE_VERSION="v(\S+)"'
          tag_prefix: 'v'
        - filename: ci_files/variables.yml
          pattern: 'GOMPLATE_VERSION: "v(\S+)"'
          tag_prefix: 'v'
      # python
      https://github.com/python/cpython.git:
        replace_in_files:
        - &python-replace
          filename: gitlab-python/Dockerfile
          pattern: 'ARG PYTHON_VERSION="(\S+)"'
          tag_prefix: 'v'
          range: '< 3.10.0'
        - <<: *python-replace
          filename: gitlab-python/Dockerfile.build.ubi8
          pattern: 'ARG PYTHON_VERSION=(\S+)'
        - <<: *python-replace
          filename: gitlab-sidekiq/Dockerfile
          pattern: 'PYTHON_TAG="(\S+)"'
        - <<: *python-replace
          filename: gitlab-toolbox/Dockerfile
          pattern: 'PYTHON_TAG=(\S+)'
        - <<: *python-replace
          filename: gitaly/Dockerfile
          pattern: 'PYTHON_TAG="(\S+)"'
        - <<: *python-replace
          filename: gitlab-webservice/Dockerfile
          pattern: 'PYTHON_TAG="(\S+)"'
        - <<: *python-replace
          filename: ci_files/variables.yml
          pattern: 'PYTHON_VERSION: "(\S+)"'
      # git-filter-repo
      https://github.com/newren/git-filter-repo.git:
        replace_in_files:
        - &git-filter-repo-replace
          filename: gitaly/Dockerfile
          pattern: 'ARG GIT_FILTER_REPO_VERSION="(\S+)"'
          tag_prefix: v
        - <<: *git-filter-repo-replace
          filename: gitaly/Dockerfile.build.ubi8
        - <<: *git-filter-repo-replace
          filename: ci_files/variables.yml
          pattern: 'GIT_FILTER_REPO_VERSION: "(\S+)"'
      # exiftool
      https://github.com/exiftool/exiftool.git:
        replace_in_files:
        - &exiftool-replace
          filename: gitlab-exiftool/Dockerfile
          pattern: 'ARG EXIFTOOL_VERSION=(\S+)'
          tag_filter:
            matching: '(\d+).(\d+)'
            sort_as: '$1.$2.0'
        - <<: *exiftool-replace
          filename: gitlab-exiftool/Dockerfile.build.ubi8
        - <<: *exiftool-replace
          filename: ci_files/variables.yml
          pattern: 'EXIFTOOL_VERSION: "(\S+)"'
      # gsutil
      https://github.com/GoogleCloudPlatform/gsutil.git:
        replace_in_files:
          - &gsutil-replace
            filename: gitlab-toolbox/Dockerfile
            pattern: 'ARG GSUTIL_VERSION="(\S+)"'
            tag_filter:
              matching: '(\d+).(\d+)'
              sort_as: '$1.$2.0'
          - <<: *gsutil-replace
            filename: gitlab-toolbox/Dockerfile.build.ubi8
            pattern: 'ARG GSUTIL_VERSION=(\S+)'
      # Golang
      https://github.com/golang/go.git:
        replace_in_files:
        - &golang-replace
          filename: gitlab-go/Dockerfile
          pattern: 'ARG GO_VERSION=(\S+)'
          tag_filter:
            matching: 'go(\d+).(\d+).(\d+)'
            sort_as: '$1.$2.$3'
            output_as: '$1.$2.$3'
          range: '< 1.22.0'
        - <<: *golang-replace
          filename: gitlab-go/Dockerfile.build.ubi8
        - <<: *golang-replace
          filename: ci_files/variables.yml
          pattern: 'GO_VERSION: "(\S+)"'
      # FIPS Golang
      https://github.com/golang-fips/go.git:
        replace_in_files:
        - &fips-go-replace
          tag_filter:
            matching: 'go(\d+).(\d+).(\d+)-1-openssl-fips'
            sort_as: '$1.$2.$3'
            output_as: '$1.$2.$3'
          range: '< 1.22.0'
          filename: gitlab-go/Dockerfile.build.fips
          pattern: 'ARG GO_FIPS_TAG=go(\S+)-1-openssl-fips'
        - <<: *fips-go-replace
          filename: ci_files/variables.yml
          pattern: 'GO_FIPS_TAG: "go(\S+)-1-openssl-fips"'
        - <<: *fips-go-replace
          filename: gitlab-go/Dockerfile.build.fips
          pattern: 'ARG GO_VERSION=(\S+)'
        - <<: *fips-go-replace
          filename: ci_files/variables.yml
          pattern: 'GO_FIPS_VERSION: "(\S+)"'
