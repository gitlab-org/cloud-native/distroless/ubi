ARG RUBY_IMAGE=
ARG UBI_IMAGE=registry.access.redhat.com/ubi8/ubi-minimal:8.9

FROM ${RUBY_IMAGE} as target

FROM ${UBI_IMAGE} as build

ARG UID=1000
ARG GITLAB_USER=git
ARG GITLAB_DATA=/var/opt/gitlab
ARG DNF_OPTS
ARG DNF_INSTALL_ROOT=/install-root
ARG DNF_OPTS_ROOT

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target / ${DNF_INSTALL_ROOT}/

ADD gitlab-rails-ee.tar.gz ${DNF_INSTALL_ROOT}/
ADD gitlab-gomplate.tar.gz ${DNF_INSTALL_ROOT}/
ADD gitlab-exiftool.tar.gz ${DNF_INSTALL_ROOT}/

COPY scripts/ ${DNF_INSTALL_ROOT}/scripts

RUN chown -R ${UID}:0 ${DNF_INSTALL_ROOT}/srv/gitlab \
    && chmod o-w ${DNF_INSTALL_ROOT}/srv/gitlab \
    && chmod -R g=u ${DNF_INSTALL_ROOT}/srv/gitlab

RUN microdnf ${DNF_OPTS} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 shadow-utils && \
    adduser -m ${GITLAB_USER} -u ${UID} -R ${DNF_INSTALL_ROOT}/

RUN microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 \
            libicu tzdata uuid gzip \
            libpng libjpeg-turbo zlib libtiff util-linux \
    && microdnf ${DNF_OPTS_ROOT} clean all \
    && mkdir -p ${DNF_INSTALL_ROOT}${GITLAB_DATA}/{.upgrade-status,data,repo,config} \
    && chown -R ${UID}:0 ${DNF_INSTALL_ROOT}${GITLAB_DATA} \
    && chmod -R ug+rwX,o-rwx ${DNF_INSTALL_ROOT}${GITLAB_DATA}/repo \
    && chmod -R ug-s ${DNF_INSTALL_ROOT}${GITLAB_DATA}/repo

RUN chown -R ${UID}:0 ${DNF_INSTALL_ROOT}/scripts ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} \
    && chmod o-w ${DNF_INSTALL_ROOT}/scripts/lib ${DNF_INSTALL_ROOT}/scripts/lib/checks \
    && chmod -R g=u ${DNF_INSTALL_ROOT}/scripts ${DNF_INSTALL_ROOT}/home/${GITLAB_USER} \
    && mv ${DNF_INSTALL_ROOT}/srv/gitlab/log/ ${DNF_INSTALL_ROOT}/var/log/gitlab/ \
    && ln -s /var/log/gitlab ${DNF_INSTALL_ROOT}/srv/gitlab/log \
    && cd ${DNF_INSTALL_ROOT}/srv/gitlab \
    && mkdir -p public/uploads \
    && chown -R ${UID}:0 public/uploads \
    && chmod 0700 public/uploads \
    && chmod o-rwx config/database.yml \
    && chmod 0600 config/secrets.yml \
    && chmod -R u+rwX builds/ shared/artifacts/ \
    && chmod -R ug+rwX shared/pages/ \
    && mkdir ${DNF_INSTALL_ROOT}/home/git/gitlab-shell \
    && chown ${UID}:0 ${DNF_INSTALL_ROOT}/home/git/gitlab-shell \
    && chmod -R g=u ${DNF_INSTALL_ROOT}/home/git/gitlab-shell \
    && ln -s /srv/gitlab/GITLAB_SHELL_VERSION ${DNF_INSTALL_ROOT}/home/git/gitlab-shell/VERSION \
    && sed -e '/host: localhost/d' -e '/port: 80/d' -i config/gitlab.yml \
    && sed -e "s/# user:.*/user: ${GITLAB_USER}/" -e "s:/home/git/repositories:${GITLAB_DATA}/repo:" -i config/gitlab.yml \
    && ldconfig -r ${DNF_INSTALL_ROOT}

FROM ${RUBY_IMAGE} as final

ARG UID=1000
ARG GITLAB_USER=git
ARG GITLAB_VERSION
ARG GITLAB_DATA=/var/opt/gitlab
ARG FIPS_MODE=0
ARG DNF_INSTALL_ROOT=/install-root

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-rails" \
      name="GitLab Rails" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Rails container for GitLab." \
      description="Rails container for GitLab."

COPY --from=build ${DNF_INSTALL_ROOT}/ /

ENV RAILS_ENV=production \
    BOOTSNAP_CACHE_DIR=/srv/gitlab/bootsnap \
    ENABLE_BOOTSNAP=1 \
    RUBYOPT="-W:no-experimental" \
    EXECJS_RUNTIME=Disabled \
    CONFIG_TEMPLATE_DIRECTORY=/srv/gitlab/config \
    UPGRADE_STATUS_DIR=${GITLAB_DATA}/.upgrade-status

# Generate bootsnap cache
RUN echo "Generating bootsnap cache"; \
    cd /srv/gitlab && \
    su ${GITLAB_USER} -s /bin/bash -c "mkdir ${BOOTSNAP_CACHE_DIR}" && \
    su ${GITLAB_USER} -s /bin/bash -p -c "bundle exec bootsnap precompile --gemfile --exclude '/srv/gitlab/config/*.yml|/srv/gitlab/public' ." && \
    chown -R ${UID}:0 ${BOOTSNAP_CACHE_DIR} && \
    chmod -R g=u ${BOOTSNAP_CACHE_DIR} && \
    du -hs ${BOOTSNAP_CACHE_DIR} ;
# exit code of this command will be that of `du`

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

VOLUME ${GITLAB_DATA}
