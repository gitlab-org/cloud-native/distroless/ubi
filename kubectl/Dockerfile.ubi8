## FINAL IMAGE ##

ARG UBI_IMAGE=registry.access.redhat.com/ubi8/ubi-minimal:8.9
ARG UBI_IMAGE_MICRO=registry.access.redhat.com/ubi8/ubi-micro:8.9

FROM ${UBI_IMAGE_MICRO} as target

FROM ${UBI_IMAGE} as build

ARG DNF_OPTS
ARG DNF_OPTS_ROOT
ARG DNF_INSTALL_ROOT=/install-root

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target / ${DNF_INSTALL_ROOT}/

ADD kubectl.tar.gz ${DNF_INSTALL_ROOT}/

RUN chmod +x ${DNF_INSTALL_ROOT}/usr/local/bin/{kubectl,yq} \
    && microdnf ${DNF_OPTS} ${DNF_OPTS_ROOT} install --best --assumeyes --nodocs --setopt=install_weak_deps=0 openssh openssl \
    && microdnf ${DNF_OPTS_ROOT} clean all \
    && rm ${DNF_INSTALL_ROOT}/usr/libexec/openssh/ssh-keysign \
    && rm -f ${DNF_INSTALL_ROOT}/var/lib/dnf/history*

FROM ${UBI_IMAGE_MICRO}

ARG KUBECTL_VERSION
ARG FIPS_MODE=0
ARG DNF_INSTALL_ROOT=/install-root

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/kubectl" \
      name="GitLab kubectl" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${KUBECTL_VERSION} \
      release=${KUBECTL_VERSION} \
      summary="GitLab kubectl used to configure Kubernetes." \
      description="GitLab kubectl used to configure Kubernetes."

COPY --from=build  ${DNF_INSTALL_ROOT}/ /

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

# Default to non-root user
USER 65534:65534
ENV HOME=/tmp/kube