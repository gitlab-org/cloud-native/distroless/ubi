variables:
  # Base distribution
  DEBIAN_IMAGE: "debian:bookworm-slim"
  UBI_IMAGE: "registry.access.redhat.com/ubi8/ubi-minimal:8.9"
  UBI_MICRO_IMAGE: "registry.access.redhat.com/ubi8/ubi-micro:8.9"
  ALPINE_IMAGE: "alpine:3.15"

  # GitLab Application Components
  GITLAB_ELASTICSEARCH_INDEXER_VERSION: "main"
  GITLAB_VERSION: "master"
  GITLAB_REF_SLUG: "master"
  GITLAB_ASSETS_TAG: "master"
  GITLAB_EXPORTER_VERSION: "15.0.0"
  GITLAB_SHELL_VERSION: "main"
  GITLAB_CONTAINER_REGISTRY_VERSION: "v4.2.0-gitlab"
  GITALY_SERVER_VERSION: "master"
  GITLAB_PAGES_VERSION: "master"
  GITLAB_KAS_VERSION: "master"
  MAILROOM_VERSION: "0.0.24"
  GITLAB_ZOEKT_WEBSERVER_VERSION: "v0.0.2-fcb27"
  GITLAB_ZOEKT_DYNAMIC_INDEXSERVER_VERSION: "v0.0.1-5f25b"
  GITLAB_ZOEKT_INDEXER_VERSION: "v0.6.2-81565"
  ZOEKT_VERSION: "fcb279ae404c0aa102121b28257143ad16e77482"
  ZOEKT_INDEXER_VERSION: "81565ece8d9deec88595314c872cf267c802d18b"

  # Runtimes & Tools
  GIT_VERSION: "2.29.0"
  GIT_FILTER_REPO_VERSION: "2.38.0"
  # Follow the Go upgrade guidelines when changing the Go version
  # link: https://docs.gitlab.com/ee/development/go_guide/go_upgrade.html
  GO_VERSION: "1.21.10"
  GO_FIPS_VERSION: "1.21.9"
  GO_FIPS_TAG: "go1.21.9-1-openssl-fips"
  # Follow the Ruby upgrade guidelines when changing the ruby version
  # link: https://docs.gitlab.com/ee/development/ruby_upgrade.html
  RUBY_VERSION: 3.1.5
  NEXT_RUBY_VERSION: 3.1.5
  RUST_VERSION: 1.73.0
  PYTHON_VERSION: "3.9.19"
  KUBECTL_VERSION: "1.30.1"
  YQ_VERSION: "4.44.1"
  PG_VERSION: "15.1"
  CA_PKG_VERSION: "20220614-r0"
  CFSSL_VERSION: "1.6.1"
  CFSSL_CHECKSUM_SHA256: "89e600cd5203a025f8b47c6cd5abb9a74b06e3c7f7f7dd3f5b2a00975b15a491"
  AWSCLI_VERSION: "1.32.101"
  S3CMD_VERSION: "2.4.0"
  GM_VERSION: "1.3.36"
  GM_CHECKSUM_SHA256: "5d5b3fde759cdfc307aaf21df9ebd8c752e3f088bb051dd5df8aac7ba7338f46"
  GITLAB_LOGGER_VERSION: "v3.0.0"
  GITLAB_LOGGER_SHA256: "b6bca21e3eba525334f4b140d9d9405181a25821365ccd9065402fa050af78d5"
  GOMPLATE_VERSION: "v3.11.7"
  GOMPLATE_SHA256: "1cae3270759babb959564b74f7e2538992751ca73f74c33f838c50168ac7caf3"
  EXIFTOOL_VERSION: "12.85"

  # Docker configuration
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375
  DOCKER_TLS_CERTDIR: ""

  # Pipeline config
  ASSETS_IMAGE_PREFIX: "gitlab-assets"
  ASSETS_IMAGE_REGISTRY_PREFIX: "registry.gitlab.com/gitlab-org"
  PREFLIGHT_IMAGE_PREFIX: "cloud-native"
  PREFLIGHT_IMAGE_REGISTRY_PREFIX: "registry.gitlab.com/gitlab-org"
  PREFLIGHT_IMAGE_TAG: "1.9.4"
  GITLAB_NAMESPACE: "gitlab-org"
  CE_PROJECT: "gitlab-foss"
  EE_PROJECT: "gitlab"
  COMPILE_ASSETS: "false"

  # installroot configuration
  DNF_INSTALL_ROOT: /install-root
  DNF_OPTS_ROOT: "--installroot=${DNF_INSTALL_ROOT} --setopt=reposdir=${DNF_INSTALL_ROOT}/etc/yum.repos.d/ --setopt=cachedir=/install-cache/ --setopt=varsdir= --config= --noplugins"

## Note
# Variables ASSETS_IMAGE_REGISTRY_PREFIX, PREFIX_IMAGE_REGISTRY_PREFIX,
# GITLAB_NAMESPACE and CE_PROJECT are overridden in the mirror of this
# project in dev.gitlab.org to match their dev counterpart locations.

## Note
# Forcing complete rebuilds requires setting two items:
# FORCE_IMAGE_BUILDS=true
# DISABLE_DOCKER_BUILD_CACHE=true

