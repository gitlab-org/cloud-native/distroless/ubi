ARG GITLAB_BASE_IMAGE
ARG UBI_IMAGE=registry.access.redhat.com/ubi8/ubi-minimal:8.9


FROM ${GITLAB_BASE_IMAGE} as target

FROM ${UBI_IMAGE} as build

ARG DNF_OPTS
ARG GITLAB_USER=git
ARG UID=1000
ARG DNF_INSTALL_ROOT=/install-root

RUN mkdir -p ${DNF_INSTALL_ROOT}
COPY --from=target / ${DNF_INSTALL_ROOT}/

ADD gitlab-gomplate.tar.gz ${DNF_INSTALL_ROOT}/
ADD gitlab-container-registry.tar.gz ${DNF_INSTALL_ROOT}/

RUN microdnf update -y && \
    microdnf ${DNF_OPTS} install --nodocs --best --assumeyes --setopt=install_weak_deps=0 shadow-utils && \
    adduser -m ${GITLAB_USER} -u ${UID} -R ${DNF_INSTALL_ROOT}/ \
    && rm -f /install-root/var/lib/dnf/history*

## FINAL IMAGE ##

FROM ${GITLAB_BASE_IMAGE}

ARG REGISTRY_VERSION
ARG GITLAB_USER=git
ARG FIPS_MODE=0
ARG DNF_INSTALL_ROOT=/install-root

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-container-registry" \
      name="Container Registry" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${REGISTRY_VERSION} \
      release=${REGISTRY_VERSION} \
      summary="The Docker toolset to pack, ship, store, and deliver content." \
      description="The Docker toolset to pack, ship, store, and deliver content. This is a fork of official Docker Registry 2.0 implementation."

COPY --from=build ${DNF_INSTALL_ROOT}/ /
COPY scripts/ /scripts/

RUN ln -sf /usr/local/bin/registry /bin/registry && \
    chgrp -R 0 /scripts /home/${GITLAB_USER} && \
    chmod -R g=u /scripts /home/${GITLAB_USER}

## Hardening: CIS L1 SCAP
RUN --mount=type=bind,rw,from=hardening,source=/,target=/hardening \
    set -ex; for f in /hardening/*.sh; do sh "$f"; done

USER ${UID}

ENV CONFIG_DIRECTORY=/etc/docker/registry
ENV CONFIG_FILENAME=config.yml
ENV GITLAB_USER=${GITLAB_USER}

ENTRYPOINT ["/scripts/entrypoint-ubi8.sh"]
CMD ["/scripts/process-wrapper"]

HEALTHCHECK --interval=30s --timeout=30s --retries=5 CMD /scripts/healthcheck
